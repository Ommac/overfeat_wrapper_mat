function ICUBWORLDopts = ICUBWORLDinit(dataset_version)

clear ICUBWORLDopts

if dataset_version==0
   
    classnames = { ...
        'bananas'
        'bottles'
        'boxes'
        'bread'
        'cans'
        'lemons'
        'pears'
        'peppers'
        'potatoes'
        'yogurt'
        };
    
    tasknames = { ...
        'background'
        'categorization'
        'demonstrator'
        'robot'
        };

elseif dataset_version==1
    
    classnames = {...
        'bottle'
        'box'
        'octopus'
        'phone'
        'pouch'
        'spray'
        'turtle'
        };

    tasknames = { ...
        ''
        };
    
elseif dataset_version==2
    
    classnames = {...
        '1_laundrydetergent'
        '2_washingupliquid'
        '3_sprinkler'
        '4_mug'
        '5_soap'
        '6_sponge'
        '7_dish'
        };
    
    ICUBWORLDopts.nobjectsperclass = 4;
    
    tasknames = { ...
        ''
        };
    
%     classnames = {...
%         '1_laundrydetergent_1'
%         '1_laundrydetergent_2'
%         '1_laundrydetergent_3'
%         '1_laundrydetergent_4'
%         '2_washingupliquid_1'
%         '2_washingupliquid_2'
%         '2_washingupliquid_3'
%         '2_washingupliquid_4'
%         '3_sprinkler_1'
%         '3_sprinkler_2'
%         '3_sprinkler_3'
%         '3_sprinkler_4'
%         '4_mug_1'
%         '4_mug_2'
%         '4_mug_3'
%         '4_mug_4'
%         '5_soap_1'
%         '5_soap_2'
%         '5_soap_3'
%         '5_soap_4'
%         '6_sponge_1'
%         '6_sponge_2'
%         '6_sponge_3'
%         '6_sponge_4'
%         '7_dish_1'
%         '7_dish_2'
%         '7_dish_3'
%         '7_dish_4'};
%     
%     classnames = {...
%         ['1_laundrydetergent_' modality(end)]
%         ['2_washingupliquid_' modality(end)]
%         ['3_sprinkler_' modality(end)]
%         ['4_mug_' modality(end)]
%         ['5_soap_' modality(end)]
%         ['6_sponge_' modality(end)]
%         ['7_dish_' modality(end)]
%         };
    
end

ICUBWORLDopts.classes = containers.Map (classnames, 1:length(classnames));  
ICUBWORLDopts.tasks = containers.Map (tasknames, 1:length(tasknames));  