%%% Subdirectories renaming tool %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%   Author: Raffaello Camoriano                                         %
%   Contact: raffaello.camoriano@iit.it                                 %
%                                                                       %
% This script renames all the subfolders in a specified path whose name %
% matches a regexp (in this case, if it contains a dot '.') and renames %
% them according to a rule (in this case, replacing the dot with a      %
% underscore '_')                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

datasetname = '256_ObjectCategories';

dataset_images_path = fullfile('/home' , '/kammo' , '/Projects' , '/LargeScaleLearn' , '/datasets/' , datasetname , 'input_img');

d = dir(dataset_images_path);
isub = [d(:).isdir]; %# returns logical vector
objlist = {d(isub).name}';
objlist(ismember(objlist,{'.','..'})) = [];

for k=1:length(objlist)
    currobj=objlist{k};
    
    [firstPart , pos] = textscan(currobj,'%[^.]');

    if pos < size(currobj , 2)
        fullString = textscan(currobj,'%s');
        secondPart = textscan(currobj(pos + 2:end),'%s');

        oldname = char(strcat(dataset_images_path , '/' , fullString{1,1}));
        newname = char(strcat(dataset_images_path , '/' ,firstPart{1} , '_' , secondPart{1}));

        movefile(oldname,newname);
    end
end

