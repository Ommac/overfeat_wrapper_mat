function [y_filt_class, ypred_filt_class, acc_filt] = filter_y(y, ypred, windows)

n_windows = length(windows);

n_classes = size(y,2);

y_filt_class = cell(n_windows, 1);
ypred_filt_class = cell(n_windows, 1);
acc_filt = cell(n_windows, 1);

[~, y_class] = max(y, [], 2);
[~, ypred_class] = max(ypred, [], 2); 
      
for idx_window=1:n_windows
      
    w = windows(idx_window);
    halfw = (w-1)/2;
    y_filt_class{idx_window} = colfilt(y_class, [w 1], 'sliding', @mode);
    ypred_filt_class{idx_window} = colfilt(ypred_class, [w 1], 'sliding', @mode);
    
    y_filt_class{idx_window}(1:halfw) = [];
    y_filt_class{idx_window}(end:-1:(end-halfw+1)) = [];
    ypred_filt_class{idx_window}(1:halfw) = [];
    ypred_filt_class{idx_window}(end:-1:(end-halfw+1)) = [];
    
    n_frames = size(y,1)-(w-1);
    y_filt = -ones(n_frames, n_classes);
    ypred_filt = -ones(n_frames, n_classes);
    
    y_indices = sub2ind(size(y_filt), 1:n_frames, y_filt_class{idx_window}');
    ypred_indices = sub2ind(size(ypred_filt), 1:n_frames, ypred_filt_class{idx_window}');
    
    y_filt(y_indices) = 1;
    ypred_filt(ypred_indices) = 1;
    
    acc_filt{idx_window} = sum(ypred_filt==y_filt, 1)/n_frames;
end

end

