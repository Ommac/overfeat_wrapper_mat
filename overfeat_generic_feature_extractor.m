%% objrecpipe_mat setup

datasetname = '256_ObjectCategories';
FEATURES_DIR = strcat('/home/kammo/Repos/overfeat_wrapper_mat');
addpath(genpath(FEATURES_DIR));

%% INPUT paths

dataset_images_path = fullfile('/home' , '/kammo' , '/Projects' , '/LargeScaleLearn' , '/datasets/' , datasetname , 'input_img');

%% OUTPUT paths

% REGISTRY files
registry_file = strcat( datasetname , '_registry.txt');
registry_path = fullfile('/home/' , 'kammo/' , 'Projects/' , 'LargeScaleLearn/' , 'datasets/' , datasetname , 'extracted_features/' , 'Overfeat/' , registry_file );

% OVERFEAT features
overfeat_path = fullfile('/home/' , 'kammo/' , 'Projects/' , 'LargeScaleLearn/' , 'datasets/' , datasetname , 'extracted_features/' , 'Overfeat');

% CLASSIFIER y
y_path = fullfile('/home/' , 'kammo/' , 'Projects/' , 'LargeScaleLearn/' , 'datasets/' , datasetname , 'extracted_features/' , 'Overfeat/' , 'Y.mat');


%% PIPELINE parameters

% DATASET parameters
dataset_extension = []; % either '[]', '.bin' or '.mat'

% OVERFEAT parameters
overfeat_install_dir = '/home/kammo/Repos/OverFeat';
overfeat_platform = 'linux_64'; % either 'linux_64' or 'linux_32' or 'macos' 
overfeat_net_model = 'small'; % either 'small' or 'large'
overfeat_out_layer = 'default'; % either 'default' 
% or an integer between 1 and the highest layer
% 'default' corresponds to layer 19 for the small and 22 for the large net
experiment_name = ['overfeat_', overfeat_net_model, '_', overfeat_out_layer];
overfeat_extension = '.mat'; 

%% automatic object list creation

d = dir(dataset_images_path);
isub = [d(:).isdir]; %# returns logical vector
objlist = {d(isub).name}';
objlist(ismember(objlist,{'.','..'})) = [];

%% PIPELINE creation

% DATASET
dataset = Features.GenericFeature(dataset_extension);

% OVERFEAT
overfeat = Features.MyOverFeat(overfeat_extension, overfeat_net_model, overfeat_out_layer, overfeat_install_dir, overfeat_platform);

%% DATASET initialization

dataset.assign_registry_and_tree_from_folder(dataset_images_path, registry_path);

%% OVERFEAT initialization and extraction

overfeat.import_registry_and_tree(dataset);

overfeat.extract('file', '', 'file', overfeat_path, dataset);

disp('OverFeat done!');

%% Y creation

dataset.create_y('file', y_path, objlist);

% if you pass 'both' to create_y you have the y also in the wspace
% and can avoid loading them
dataset.load_y(y_path);
