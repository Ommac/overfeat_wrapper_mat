classdef MyOverFeat < Features.GenericFeature
    
    properties
        
        NetModel
        OutLayer
        
        InstallDir
        Platform
        
    end
    
    methods
        
        function obj = MyOverFeat(ext, net_model, out_layer, install_dir, platform)
            
            obj = obj@Features.GenericFeature(ext);
            
            if strcmp(net_model, 'large')
                obj.NetModel = '-l';
            elseif strcmp(net_model, 'small')
                obj.NetModel = '';
            else
                error('Invalid network type.');
            end
            
            if strcmp(out_layer, 'default')
                obj.OutLayer = '';
            else 
                obj.OutLayer = out_layer;
            end
            
            obj.InstallDir = install_dir;
            obj.Platform = platform;

        end
        
        function [feat, grid, src_size, im_size] = extract_image(obj, mod_in, mod_out, src, src_grid, im_size, dst)
          
            if (strcmp(mod_in,'wspace'))
                error('OverFeat can only process images from disk.');
            end

            im_size = 1;
            
            if (strcmp(mod_in,'file'))
                
               [src_path, src_name, src_ext] = fileparts(src);
               
               if strcmp(src_ext, '.txt')
                   
                   feat = fileread(src);
                   feat = str2num(feat(10:end))';    
                   
                   src_size = size(feat);
               else
                   % See www.imagemagick.org/Usage/resize/ for cropping
                   % information
                   
                   if isempty(obj.OutLayer)
                       command = sprintf('%s/bin/%s/overfeat_fill_crop %s -f %s', obj.InstallDir, obj.Platform, obj.NetModel, src);
                   else
                       command = sprintf('%s/bin/%s/overfeat_fill_crop %s -L %s %s', obj.InstallDir, obj.Platform, obj.NetModel, num2str(obj.OutLayer), src);
                   end
                   [status, feat] = system(command);
                   
                   %grid = str2num(feat(1:8));
                   [C , pos] = textscan(feat,'%d',3);
                   grid = cell2mat(C);
                   
                   %feat = str2num(feat(10:end))';
                   feat = cell2mat(textscan(feat(pos + 1:end),'%f'));

                   src_size = im_size;
                   
               end
            end
            % n*h*w floating point numbers (written in ascii) separated by spaces
            % number of features (n)
            % number of rows (h)
            % number of columns (w)
            
            % the feature is the first dimension (so that to obtain the next feature, you must add w*h to your index)
            % followed by the row (to obtain the next row, add w to your index)
            % that means that if you want the features corresponding to the top-left window, you need to read pixels i*h*w for i=0..4095
            
            % the output is going to be a 3D tensor
            % the first dimension correspond to the features
            % while dimensions 2 and 3 are spatial (y and x respectively)
            % the spatial dimension is reduced at each layer,
            % and, with the default network, using option -f,
            % the output has size nFeatures * h * w where
            %
            % for the small network,
            % nFeatures = 4096
            % h = ((H-11)/4 + 1)/8-6
            % w = ((W-11)/4 + 1)/8-6
            %
            % for the large network,
            % nFeatures = 4096
            % h = ((H-7)/2 + 1)/18-5
            % w = ((W-7)/2 + 1)/18-5
            %
            % if the input has size 3*H*W
            % each pixel in the feature map corresponds to a localized window in the input
            % with the small network, the windows are 231x231 pixels,
            % overlapping so that the i-th window begins at pixel 32*i,
            % while for the large network, the windows are 221x221,
            % and the i-th window begins at pixel 36*i.
            %
            
            feat_size = size(feat);           
            grid_size = size(grid);
            
            if (strcmp(mod_out,'file') || strcmp(mod_out,'both'))
                
                if strcmp(obj.Ext,'.bin')
                    fid = fopen(dst, 'w', 'L');
                    % append modality
                    % column major order (transpose before writing)
                    % format for Unix systems (i.e., L, big endian)
                    fwrite(fid, im_size, 'double');
                    fclose(fid);
                    fid = fopen(dst, 'a', 'L');
                    fwrite(fid, feat_size, 'double');
                    fwrite(fid, feat, 'double');
                    fwrite(fid, grid_size, 'double');
                    fwrite(fid, grid, 'double');
                    fclose(fid);
                elseif strcmp(obj.Ext,'.mat')
                    save(dst,'feat', 'grid', 'feat_size', 'grid_size', 'im_size');
                else
                    error('Error! Invalid extension.');
                end
                
            end
        end
        
    end

end

