classdef GenericFeature < handle %matlab.mixin.Copyable %hgsetget 
    
    properties
       
        Ext

        Tree
        Registry
        RegistryPath
        
        RootPath
        Feat
        FeatSize
        ExampleCount
        
        ImSize
        Grid
        GridSize
      
        nRandFeatures
        Dictionary
        DictionarySize
        DictionaryPath
        
        Y
        YPath
    end
    
    methods
        
        function obj = GenericFeature(extension)
            
            obj.Ext = extension;
        end
        
        % in case one just wants to extract features from the whole dataset
        % it is possible to copy from input object
        % - copy .Tree
        % - copy .ExampleCount
        % - copy .Registry
        % - copy .RegistryPath
        function import_registry_and_tree(object, prev_object) 
            
            % set(object, 'Tree', prev_object.Tree);
            % set(object, 'ExampleCount', prev_object.ExampleCount);
            % set(object, 'Registry', prev_object.Registry);
            % set(object, 'RegistryPath', prev_object.RegistryPath);
            object.Tree = prev_object.Tree;
            object.ExampleCount = prev_object.ExampleCount;
            object.Registry = prev_object.Registry;
            if ~isempty(object.Ext)
                object.Registry = regexp(prev_object.Registry,'\.','split');
                object.Registry = cellfun(@(x) x(1,1), object.Registry);
                object.Registry = strcat(object.Registry, object.Ext);
            end
            object.RegistryPath = prev_object.RegistryPath;

        end
        
        % scan the path of folders and
        % - assign .RootPath = path
        % - assign .RegistryPath = reg_path and create registry file
        % - assign .Registry
        % - assign .Tree
        % - assign .ExampleCount
        function assign_registry_and_tree_from_folder(object, root_path, out_reg_path)
            
            % Init members
            object.RootPath = root_path;
            
            object.RegistryPath = [];
            
            object.Registry = [];
            
            object.Tree = struct('name', {}, 'subfolder', {});
            object.ExampleCount = 0;
            
            % explore folders creating .Tree, registry file and .Registry
            object.Tree = explore_next_level_folder(object, '', object.Tree);
            
            if ~isempty(out_reg_path)
                fid = fopen(out_reg_path,'w');
                if (fid==-1)
                    error('Cannot open output registry file.');
                end
                object.RegistryPath = out_reg_path;
                for line_idx=1:object.ExampleCount
                    fprintf(fid, '%s\n', object.Registry{line_idx});
                end
                fclose(fid);
            end
            
        end
        function current_level = explore_next_level_folder(obj, current_path, current_level)
            
            % get the listing of files at the current level
            files = dir(fullfile(obj.RootPath, current_path));
            
            for idx_file = 1:size(files)
                
                % for each folder, create its duplicate in the hierarchy
                % then get inside it and repeat recursively
                if (files(idx_file).name(1)~='.')
                    
                    if (files(idx_file).isdir)
                        
                        tmp_path = current_path;
                        current_path = fullfile(current_path, files(idx_file).name);
                        
                        current_level(length(current_level)+1).name = files(idx_file).name;
                        current_level(length(current_level)).subfolder = struct('name', {}, 'subfolder', {});
                        current_level(length(current_level)).subfolder = explore_next_level_folder(obj, current_path, current_level(length(current_level)).subfolder);
                        
                        % fall back to the previous level
                        current_path = tmp_path;
                        
                    else
                        
                        % for each image put its path in the registry file
                        if isempty(obj.Ext)
                            file_src = fullfile(current_path, files(idx_file).name);
                        else
                            file_src = fullfile(current_path, files(idx_file).name);
                            [file_folder, file_name, file_ext] = fileparts(file_src);
                            file_src = fullfile(file_folder, file_name, obj.Ext);
                        end
                        obj.ExampleCount = obj.ExampleCount + 1;
                        obj.Registry{obj.ExampleCount,1} = file_src;
                    end
                end
            end
        end
                
        function reproduce_tree(obj, path)
            reproduce_level(obj, path, obj.Tree);
        end
        function reproduce_level(obj, current_path, current_level)
            
            for idx=1:length(current_level)
                
                tmp_path = current_path;
                current_path = fullfile(current_path, current_level(idx).name);
                if ~isdir(current_path)
                    mkdir(current_path);
                end
                reproduce_level(obj, current_path, current_level(idx).subfolder);
                % fall back to the previous level
                current_path= tmp_path;
            end
        end
        
        % read an input registry file and select only specified folders
        % - create .Tree
        % - create .ExampleCount
        % - create .Registry
        % - create .RegistryPath
        function assign_registry_and_tree_from_file(object, input_reg_path, folders_list, out_reg_path)

            object.RegistryPath = [];
            
            input_registry = textread(input_reg_path, '%s', 'delimiter', '\n'); 
   
            % extract only path
            paths = cellfun(@fileparts, input_registry, 'UniformOutput', false);
            
            % for branches, keep only different paths and separate folder names
            branches = unique(paths);
            branches = regexp(branches, ['\' filesep], 'split');
  
            % select only specified folders
            if ~isempty(folders_list)
                
                % for files, separate folder names
                files = regexp(paths, ['\' filesep], 'split');
                
                f_indices = zeros(length(input_registry),1);
                b_indices = zeros(length(branches),1);
                for idx_folder=1:length(folders_list)
                    b_indices = b_indices + sum(cell2mat(cellfun(@(x) strcmp(x,folders_list{idx_folder}), branches, 'UniformOutput', false)),2);
                    f_indices = f_indices + sum(cell2mat(cellfun(@(x) strcmp(x,folders_list{idx_folder}), files, 'UniformOutput', false)),2);
                end
                
                branches = branches(b_indices~=0);
                object.Registry = input_registry(f_indices~=0);
            else
                object.Registry = input_registry;
            end
            if ~isempty(object.Ext)
                object.Registry = regexp(object.Registry,'\.','split');
                object.Registry = cellfun(@(x) x(1,1), object.Registry);
                object.Registry = strcat(object.Registry, object.Ext);
            end
            
            object.ExampleCount = size(object.Registry,1);

            % convert column cell-array of row cell-arrays into cell matrix
            nbranches = length(branches);
            branch_deepness = zeros(nbranches,1);
            for idx1=1:nbranches
                branch_deepness(idx1) = size(branches{idx1},2);
            end
            folders = cell(nbranches, max(branch_deepness));
            for idx1=1:nbranches
                for idx2=1:branch_deepness(idx1) 
                    folders{idx1,idx2} = branches{idx1}{idx2};
                end
            end
            folders(cellfun('isempty',folders)) = {'-1'};
            
            % convert cell-matrix into row cell-array of column cell-arrays
            levels = cell(1,max(branch_deepness));
            for idx2=1:max(branch_deepness)
                %levels{idx2} = folders( find(~cellfun('isempty',folders(:,idx2))), idx2 );
                levels{idx2} = folders( :, idx2 );
            end
            
            % create .Tree
            object.Tree = struct('name', {}, 'subfolder', {});
            current_level_idx = 1;
            
            folder_list = unique(levels{current_level_idx});
            for f=1:length(folder_list)
                object.Tree(f).name = folder_list{f};
                object.Tree(f).subfolder = struct('name', {}, 'subfolder', {});
                selected_branches = strcmp(folder_list{f},levels{current_level_idx});
                object.Tree(f).subfolder = explore_next_level_file(object, levels, selected_branches, current_level_idx+1, object.Tree(f).name, object.Tree(f).subfolder);
            end
            
            if ~isempty(out_reg_path)
                fid = fopen(out_reg_path,'w');
                if fid==-1
                    error('Cannot open output registry file.');
                end
                object.RegistryPath = out_reg_path;
                for line_idx=1:object.ExampleCount
                    fprintf(fid, '%s\n', object.Registry{line_idx});
                end  
                fclose(fid);
            end
        end
        function level_tree = explore_next_level_file(obj, levels, selected_branches_upper, level_idx, upper_folder, level_tree)
            
            if level_idx>length(levels)
                return
            end
            folder_list = unique( levels{level_idx}(selected_branches_upper) );
            folder_list(strcmp(folder_list, '-1')) = [];
            for f=1:length(folder_list)
                level_tree(f).name = folder_list{f};
                level_tree(f).subfolder = struct('name', {}, 'subfolder', {});
                selected_branches = selected_branches_upper & strcmp(folder_list{f}, levels{level_idx});
                level_tree(f).subfolder = explore_next_level_file(obj, levels, selected_branches, level_idx+1, level_tree(f).name, level_tree(f).subfolder);
            end
            
        end
        
        % if modality_in = 'file' it loads features from prev_obj.RootPath
        % using prev_obj.Registry
        % if modality_in = 'wspace' it uses prev_obj.Feat
        % --> these fields must be set correctly
        % no need for dictionary
        % if modality_out = 'file' it assigns obj.RootPath = path
        % if modality_out = 'wspace' it assigns obj.Feat
        % EXAMPLE:
        % obj.extract(modality_in, '', modality_out, sift_path, prev_obj);
        % with modality_in = 'file'
        % and modality_out = {'file', 'wspace', 'both'}
        function extract(object, modality_in, modality_dict, modality_out, path, prev_object)
            
            % Preallocate output variables if needed
            if (strcmp(modality_in,'file'))
                if isempty(prev_object.RootPath)
                    error('Error! Missing RootPath in previous object for loading features.');
                end
                if isempty(prev_object.Registry)
                    error('Error! Missing Registry in previous object for loading features.');
                end
                prev_object.FeatSize = [];
                prev_object.ImSize = zeros(2, object.ExampleCount);
            end
            
            if (strcmp(modality_in, 'wspace'))
                if isempty(prev_object.Feat)
                    error('Error! Missing Feat in previous object.');
                end
            end
            
            if (strcmp(modality_out,'wspace') || strcmp(modality_out,'both'))
                object.FeatSize = zeros(2, object.ExampleCount);
                object.ImSize = zeros(2, object.ExampleCount);
                object.GridSize = zeros(2, object.ExampleCount);
                object.Grid = [];
                object.Feat = [];
            end
            
            if (strcmp(modality_out,'file') || strcmp(modality_out,'both'))
                if ~isdir(path)
                    mkdir(path);
                end
                object.RootPath = path;
                object.reproduce_tree(path);
            end
            
            if (strcmp(modality_dict, 'file'))
                if isempty(object.DictionaryPath)
                    error('Error! Missing DictionaryPath in current object for loading dictionary.');
                end
                object.load_dictionary(object.DictionaryPath);
            end
            
            if (strcmp(modality_dict, 'wspace'))
                if isempty(object.Dictionary)
                    error('Error! Missing Dictionary in current object.');
                end
            end
            
            for idx_feat = 1:object.ExampleCount
                
                if (strcmp(modality_in,'file') || strcmp(modality_out,'file') || strcmp(modality_out,'both'))
                    out_relative_path = object.Registry{idx_feat};
                    in_relative_path = prev_object.Registry{idx_feat};
                    
                    if (strcmp(modality_in,'file'))
                        feat_src = fullfile(prev_object.RootPath, in_relative_path);
                        feat_src_grid = '';
                        im_size = [];
                    end
                    if (strcmp(modality_out,'file') || strcmp(modality_out,'both'))
                        file_dst = fullfile(object.RootPath, out_relative_path);
                    end
                end
                if (strcmp(modality_in,'wspace'))
                    feat_src = prev_object.Feat(:, (sum(prev_object.FeatSize(2, 1:(idx_feat-1)))+1):sum(prev_object.FeatSize(2, 1:idx_feat)));
                    feat_src_grid = prev_object.Grid(:, (sum(prev_object.GridSize(2, 1:(idx_feat-1)))+1):sum(prev_object.GridSize(2, 1:idx_feat)));
                    im_size = prev_object.ImSize(:,idx_feat);
                end
                if (strcmp(modality_out,'wspace'))
                    file_dst = '';
                end
                [feat_dst, feat_dst_grid, feat_src_size, im_size] = extract_image(object, modality_in, modality_out, feat_src, feat_src_grid, im_size, file_dst);
                
                if (strcmp(modality_in,'file'))
                    if (~isempty(feat_src_size))
                        prev_object.FeatSize = [prev_object.FeatSize feat_src_size];
                    end
                    prev_object.ImSize(:, idx_feat) = im_size;
                end
                if (strcmp(modality_out,'wspace') || strcmp(modality_out,'both'))
                    object.FeatSize(:, idx_feat) = size(feat_dst);
                    object.ImSize(:, idx_feat) = im_size;
                    object.GridSize(:, idx_feat) = size(feat_dst_grid);
                    object.Grid(:,(end+1):(end+size(feat_dst_grid,2))) = feat_dst_grid;
                    object.Feat(:,(end+1):(end+size(feat_dst,2))) = feat_dst;
                end
                
                if mod(idx_feat,1)==0
                    disp([num2str(idx_feat) '/' num2str(object.ExampleCount)]);
                end
            end
            
        end
        
        function load_feat_rndsubset(object, path, n_rand_feat)
            
            object.RootPath = path;
           
            if isempty(object.Registry)
               error('Error! Missing Registry in previous object for loading features.');
            end
        
            % clear feature matrix
            object.Feat = [];
          
            % init feature size (= size of feature matrix for each image)  
            object.FeatSize = zeros(2,object.ExampleCount);
            
            % deletes grid information because loads features randomly
            object.GridSize = [];
            object.Grid = [];

            % load first only feature sizes
            for line_idx=1:object.ExampleCount
                relative_path = object.Registry{line_idx};
                
                if strcmp(object.Ext,'.bin')
                    fid = fopen(fullfile(object.RootPath, relative_path), 'r', 'L');
                    % append modality
                    % column major order
                    % format for Unix systems (i.e., L, big endian)
                    im_size = fread(fid, [2 1], 'double');
                    object.FeatSize(:,line_idx) = fread(fid, [2 1], 'double');
                    fclose(fid);
                elseif strcmp(object.Ext,'.mat')
                    input = load(fullfile(object.RootPath, relative_path),'-mat', 'feat_size');
                     object.FeatSize(:,line_idx) = input.feat_size;
                else
                    error('Error! Invalid extension.');
                end
            end
            
            % compute overall number of features in the dataset...
            n_feat = sum(object.FeatSize(2,:));
            
            % ...to get a range in which extract random indices
            if n_rand_feat<n_feat
                rnd_indices = vl_colsubset(1:n_feat, n_rand_feat);
                object.nRandFeatures = n_rand_feat;
            else 
                rnd_indices = 1:n_feat;
                object.nRandFeatures = n_feat;
            end
            
            % store selected features
            start_idx = 0;
            for line_idx=1:object.ExampleCount
                relative_path = object.Registry{line_idx};
  
                end_idx = start_idx + object.FeatSize(2,line_idx);
                selected_indices = rnd_indices(rnd_indices>start_idx & rnd_indices<=end_idx) - start_idx;
                start_idx = end_idx;
                
                if strcmp(object.Ext,'.bin')
                    
                    fid = fopen(fullfile(object.RootPath, relative_path), 'r', 'L');
                    % append modality
                    % column major order 
                    % format for Unix systems (i.e., L, big endian)
                    im_size = fread(fid, [2 1], 'double');
                    feat_size = fread(fid, [2 1], 'double');
                    feat = fread(fid, feat_size', 'double'); 
                    object.Feat(:, (end+1):(end+size(selected_indices,2))) = feat(:,selected_indices);
                    fclose(fid);
                    
                elseif strcmp(object.Ext,'.mat')
                    input = load(fullfile(object.RootPath, relative_path),'-mat', 'feat');
                    object.Feat(:,(end+1):(end+size(selected_indices,2))) = input.feat(:,selected_indices);
                else
                    error('Error! Invalid extension.');
                end
   
            end  

        end 
        function load_feat(object, path)
            
            object.RootPath = path;
           
            if isempty(object.Registry)
               error('Error! Missing Registry in previous object for loading features.');
            end
                
            object.FeatSize = zeros(2, object.ExampleCount);
            object.GridSize = zeros(2, object.ExampleCount);
            object.Grid = [];
            object.Feat = [];
            
            for line_idx=1:object.ExampleCount
                relative_path = object.Registry{line_idx};
                
                if strcmp(object.Ext,'.bin')
                    
                    fid = fopen(fullfile(object.RootPath, relative_path), 'r', 'L');
                    % append modality
                    % column major order 
                    % format for Unix systems (i.e., L, big endian)
                    object.ImSize(:, line_idx) = fread(fid, [2 1], 'double');
                    object.FeatSize(:, line_idx) = fread(fid, [2 1], 'double');
                    object.Feat(:,(end+1):(end+object.FeatSize(2, line_idx))) = fread(fid, object.FeatSize(:, line_idx)', 'double');
                    object.GridSize(:, line_idx) = fread(fid, [2 1], 'double');
                    object.Grid(:,(end+1):(end+object.GridSize(2, line_idx))) = fread(fid, object.GridSize(:, line_idx)', 'double');
                    fclose(fid);
                    
                elseif strcmp(object.Ext,'.mat')
                    
                    input = load(fullfile(object.RootPath, relative_path), 'feat', 'grid', 'feat_size', 'grid_size');
                    object.FeatSize(:, line_idx) = input.feat_size;
                    object.GridSize(:, line_idx) = input.grid_size;
                    object.Grid(:,(end+1):(end+input.grid_size(2))) = input.grid;
                    object.Feat(:,(end+1):(end+input.feat_size(2))) = input.feat;
                    
                else
                    error('Error! Invalid extension.');
                end
   
            end  

        end
        function save_feat(object, path)
              
            registry_fid = fopen(object.RegistryPath,'r');
            if (registry_fid==0)
                error('Error! Please provide a valid path for dataset registry.');
            end
            
            for line_idx=1:object.ExampleCount
                relative_path = fgetl(registry_fid);
                if ischar(relative_path)
                    feat_size = object.FeatSize(:, line_idx);
                    grid_size = object.GridSize(:, line_idx);
                    grid = object.Grid(:, (sum(prev_object.GridSize(2, 1:(line_idx-1)))+1):sum(object.GridSize(2, 1:line_idx)));
                    feat = object.Feat(:, (sum(prev_object.FeatSize(2, 1:(line_idx-1)))+1):sum(object.FeatSize(2, 1:line_idx)));
                    
                    if strcmp(object.Ext,'.bin')
                        fid = fopen(fullfile(path, [relative_path object.Ext]), 'w', 'L');
                        % append modality
                        % column major order
                        % format for Unix systems (i.e., L, big endian)
                        fwrite(fid, im_size, 'double');
                        fclose(fid);
                        fid = fopen(fullfile(path, [relative_path object.Ext]), 'a', 'L');
                        fwrite(fid, feat_size, 'double');
                        fwrite(fid, feat, 'double');
                        fwrite(fid, grid_size, 'double');
                        fwrite(fid, grid, 'double');
                        fclose(fid);
                    elseif strcmp(obj.Ext,'.mat')
                        save(fullfile(path, [relative_path object.Ext]),'feat', 'grid', 'feat_size', 'grid_size', 'im_size');
                    else
                        error('Error! Invalid extension.');
                    end
                else
                error('Error! Not valid registry file entries.');
                end  
            end   
            fclose(registry_fid);   
            
        end
        function save_feat_matrix(object, path)
            
            feat_size = object.FeatSize;
            feat = object.Feat;
            
            if strcmp(object.Ext,'.bin')
                fid = fopen(path, 'w', 'L');
                % append modality
                % column major order 
                % format for Unix systems (i.e., L, big endian)
                fwrite(fid, feat_size, 'double');
                fclose(fid);
                fid = fopen(path, 'a', 'L');
                fwrite(fid, feat, 'double');
                fclose(fid);
            elseif strcmp(object.Ext,'.mat')
                save(path, 'feat', 'feat_size');
            else
                error('Error! Invalid extension.');
            end
            
        end
        function load_feat_matrix(object, path)
            
            if strcmp(object.Ext,'.bin')
                fid = fopen(path, 'r', 'L');
                % append modality
                % column major order
                % format for Unix systems (i.e., L, big endian)
                object.FeatSize = fread(fid, [2 1], 'double');
                object.Feat = fread(fid, object.FeatSize', 'double');
                fclose(fid);
            elseif strcmp(obj.Ext,'.mat')
                
                input = load(path, '-mat', 'feat_size', 'feat');
                if isfield(input, 'feat') && ~isempty(input.feat)
                    object.FeatSize = input.feat_size;
                    object.Feat = input.feat;
                else
                    error('Error! Could not load .mat dictionary from specified path.');
                end
            else
                error('Error! Invalid extension.');
            end
        end
        
        % if modality_in = 'file' it loads features from prev_obj.RootPath using prev_obj.Registry
        % if modality_in = 'wspace' it uses prev_obj.Feat
        % --> these fields must be set correctly
        % if modality_out = 'file' it assigns obj.DictionaryPath = path
        % if modality_out = 'wspace' it assigns obj.Dictionary
        % EXAMPLE:
        % obj.dictionarize(modality_in, modality_out, obh_path, prev_object);
        % with modality_in = {'file', 'wspace'}
        % and modality_out = {'file', 'wspace', 'both'}
        function dictionarize(object, modality_in, n_rand_feat, modality_out, path, prev_object)
            
            % Load previous features in the wspace if needed
            if (strcmp(modality_in,'file'))
                if isempty(prev_object.RootPath)
                    error('Error! Missing RootPath in previous object for loading features.');
                end
                prev_object.load_feat_rndsubset(prev_object.RootPath, n_rand_feat);
            end
            
            if isempty(prev_object.Feat)
                error('Error! Could not find/load previous features in wspace.');
            end
            
            % Prepare output folder if needed
            if (strcmp(modality_out,'file') || strcmp(modality_out,'both'))
                if ~isdir(path)
                    mkdir(path);
                end
                object.DictionaryPath = fullfile(path, ['dictionary' object.Ext]);
                file_dst = object.DictionaryPath;
            end
            
            if (strcmp(modality_out,'wspace'))
                file_dst = '';
            end
            if (strcmp(modality_out,'wspace') || strcmp(modality_out,'both'))
                object.Dictionary = object.dictionarize_matrix(prev_object.Feat, modality_out, file_dst);
                object.DictionarySize = size(object.Dictionary);
            end
            if (strcmp(modality_out,'file'))
                object.dictionarize_matrix(prev_object.Feat, modality_out, file_dst);
            end
            
        end
        
        function load_dictionary(object, path)

            [dict_folder, dict_filename, dict_ext] = fileparts(path);
            
            if strcmp(dict_ext,'.bin')
                    fid = fopen(path, 'r', 'L');
                    % append modality
                    % column major order 
                    % format for Unix systems (i.e., L, big endian)
                    object.DictionarySize = fread(fid, [2 1], 'double');
                    object.Dictionary = fread(fid, object.DictionarySize', 'double');
                    object.DictionaryPath = path;
                    fclose(fid);
            elseif strcmp(dict_ext,'.mat')
                
                input = load(path, '-mat', 'dictionary', 'dictionary_size');
                if isfield(input, 'dictionary') && ~isempty(input.dictionary)
                    object.DictionarySize = input.dictionary_size;
                    object.Dictionary = input.dictionary;
                    object.DictionaryPath = path;
                else
                    error('Error! Could not load .mat dictionary from specified path.');
                end
            elseif strcmp(dict_ext, '.txt')
               
                dict_string = dlmread(path);
                object.DictionarySize = dict_string(1,1:2);
                object.Dictionary = dict_string(2:end,:);
                object.DictionaryPath = path;
               
            else
                error('Error! Invalid extension.');
            end
            
        end
        function save_dictionary(object, path)
            
            dictionary_size = object.DictionarySize;
            dictionary = object.Dictionary;
            
            if strcmp(obj.Ext,'.bin')
                fid = fopen(path, 'w', 'L');
                % append modality
                % column major order 
                % format for Unix systems (i.e., L, big endian)
                fwrite(fid, dictionary_size, 'double');
                fclose(fid);
                fid = fopen(path, 'a', 'L');
                fwrite(fid, dictionary, 'double');
                fclose(fid);
            elseif strcmp(obj.Ext,'.mat')
                save(path, 'dictionary', 'dictionary_size');
            else
                error('Error! Invalid extension.');
            end
            
        end
        
        % copy the input obj.Dictionary and obj.DictionaryPath
        % in the same fields of calling object
        function import_dictionary(object, source_object, modality_in)
            
            if strcmp(modality_in,'file')
                object.DictionaryPath = source_object.DictionaryPath;
            elseif strcmp(modality_in, 'wspace')
                object.Dictionary = source_object.Dictionary;
                object.DictionarySize = source_object.DictionarySize;
            end
        end

        function create_y(object, modality_out, output_path, classes_list)
            
            if (isempty(object.Registry))
                error('Error! Missing Registry in calling object.');
            end
 
            nclasses = length(classes_list);
             
            object.Y = -ones(object.ExampleCount, nclasses);
            
            folder_names = regexp(object.Registry, ['\' filesep], 'split');
                
            true_classes = zeros(object.ExampleCount,1);
                for idx_class=1:nclasses
                    true_classes = true_classes + idx_class*(sum(cell2mat(cellfun(@(x) strcmp(x,classes_list{idx_class}), folder_names, 'UniformOutput', false)),2)~=0);
                end
                
            object.Y( sub2ind(size(object.Y),1:object.ExampleCount,true_classes') ) = 1;

            if (strcmp(modality_out,'file') || strcmp(modality_out,'both'))
                
                [~, ~, out_ext] = fileparts(output_path);
                object.YPath = output_path;
                y = object.Y;
                n = object.ExampleCount;
                
                if strcmp(out_ext,'.bin')
                    fid = fopen(output_path, 'w', 'L');
                    % append modality
                    % column major order
                    % format for Unix systems (i.e., L, big endian)
                    fwrite(fid, n, 'double');
                    fclose(fid);
                    fid = fopen(output_path, 'a', 'L');
                    fwrite(fid, nclasses, 'double');
                    fwrite(fid, y, 'double');
                    fclose(fid);
                elseif strcmp(out_ext,'.mat')
                    save(output_path, 'n', 'nclasses', 'y');
                else
                    error('Error! Invalid extension.');
                end 
                
                if strcmp(modality_out, 'file')
                    object.Y = [];
                end
            end
            
        end
        
        function load_y(object, y_path)
            
                [~, ~, y_ext] = fileparts(y_path);
                if strcmp(y_ext,'.bin')
                    fid = fopen(y_path, 'r', 'L');
                    % append modality
                    % column major order
                    % format for Unix systems (i.e., L, big endian)
                    object.ExampleCount = fread(fid, 1, 'double');
                    nclasses = fread(fid, 1, 'double');
                    object.Y = fread(fid, [object.ExampleCount nclasses], 'double');
                    fclose(fid);
                elseif strcmp(y_ext,'.mat')
                    input = load(y_path, 'n', 'y');
                    object.ExampleCount = input.n;
                    object.Y = input.y;
                else
                    error('Error! Invalid extension.');
                end
            
%             test_boolean = strncmp('test',imgsets, 4);
%             if sum(test_boolean)
%                 
%                 imgset = 'test';
%                 
%                 for test_idx = find(test_boolean);
%                     
%                     if (length(imgsets{test_idx})>4)
%                         task = strsplit(imgsets{test_idx}, imgsets{test_idx}(5));
%                         task = task{2};
%                         if ~isempty(task)
%                             task_no = DATASETopts.tasks(task);
%                             input_file = fullfile(path, ['Y' imgset '_' task]);
%                         else
%                             error('Invalid test task.');
%                         end
%                     else
%                         task_no = 1;
%                         input_file = fullfile(path, ['Y' imgset]);
%                     end
%                     
%                     if strcmp(extension,'.bin')
%                         fid = fopen([input_file extension], 'r', 'L');
%                         % append modality
%                         % column major order
%                         % format for Unix systems (i.e., L, big endian)
%                         object.N_test{task_no} = fread(fid, 1, 'double');
%                         object.n_classes = fread(fid, 1, 'double');
%                         object.Y_test{task_no} = fread(fid, [object.N_test{task_no} object.n_classes], 'double');
%                         fclose(fid);
%                     elseif strcmp(extension,'.mat')
%                         input = load([input_file extension], 'N', 'Nclasses', 'Y');
%                         object.N_test{task_no} = input.N;
%                         object.n_classes = input.Nclasses;
%                         object.Y_test{task_no} = input.Y;
%                     else
%                         error('Error! Invalid extension.');
%                     end
%                     
%                 end
%             end

        end
           
    end
end

