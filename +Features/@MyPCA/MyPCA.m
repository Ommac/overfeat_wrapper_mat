classdef MyPCA < Features.GenericFeature
   
    properties
        DictionaryDim
    end
    
    methods
        function obj = MyPCA(ext, dict_dim)
            
            obj = obj@Features.GenericFeature(ext);
            obj.DictionaryDim = dict_dim;
        end
        
        function [feat, grid, src_feat_size, im_size] = extract_image(obj, mod_in, mod_out, src, src_grid, im_size, dst)
            
            if isempty(obj.Dictionary)
                error('Error! Missing Dictionary in calling object.');
            end
            
            if (strcmp(mod_in,'file'))
                
               [im_path, im_name, im_ext] = fileparts(src);
                if strcmp(im_ext,'.bin')
                    
                    fid = fopen(src, 'r', 'L');
                    % append modality
                    % column major order
                    % format for Unix systems (i.e., L, big endian)
                    im_size = fread(fid, [2 1], 'double');
                    src_feat_size = fread(fid, [2 1], 'double');
                    src_feat = fread(fid, src_feat_size', 'double');
                    grid_size = fread(fid, [2 1], 'double');
                    grid = fread(fid, grid_size', 'double');
                    fclose(fid);
                    
                elseif strcmp(im_ext, '.mat')
                    
                    input = load(src, '-mat', 'feat', 'grid', 'feat_size', 'grid_size', 'im_size');
                    src_feat = input.feat;
                    src_feat_size = input.feat_size;
                    grid = input.grid;
                    grid_size = input.grid_size;
                    im_size = input.im_size;
 
                else
                    error('Error! Invalid extension.');
                end
 
            end
            if (strcmp(mod_in,'wspace'))
                src_feat = src;
                src_feat_size = size(src_feat);
                grid = src_grid; 
                grid_size = size(grid);
            end
            
            % Just project on the first FeatSize principal components
            if size(obj.Dictionary,1) == size(src_feat,1) && size(obj.Dictionary,2) == obj.DictionaryDim
               feat = obj.Dictionary' * src_feat;
            else 
                error('Error! Please check dictionary and input feature size.');
            end
            
            if (strcmp(mod_out,'file') || strcmp(mod_out,'both'))
                feat_size = size(feat);
                
                if strcmp(obj.Ext,'.bin')
                    fid = fopen(dst, 'w', 'L');
                    % append modality
                    % column major order
                    % format for Unix systems (i.e., L, big endian)
                    fwrite(fid, im_size, 'double');
                    fclose(fid);
                    fid = fopen(dst, 'a', 'L');
                    fwrite(fid, feat_size, 'double');
                    fwrite(fid, feat, 'double');
                    fwrite(fid, grid_size, 'double');
                    fwrite(fid, grid, 'double');
                    fclose(fid);
                elseif strcmp(obj.Ext,'.mat')
                    save(dst,'feat', 'grid', 'feat_size', 'grid_size', 'im_size');
                else
                    error('Error! Invalid extension.');
                end
   
            end
            
        end
            
        function dictionary = dictionarize_matrix(obj, features, mod_out, dst)
        
            if (obj.DictionaryDim < size(features,1))
              
                % dictionarize
                [dictionary,~,~] = svd(features,'econ');
                
                % keep only the first FeatSize columns
                dictionary(:,(obj.DictionaryDim+1):end) = [];
            else
                dictionary = eye(size(features,1));
            end
            
            dictionary_size = size(dictionary);

            if (strcmp(mod_out, 'file') || strcmp(mod_out, 'both'))
                
                if strcmp(obj.Ext,'.bin')
                    fid = fopen(dst, 'w', 'L');
                    % append modality
                    % column major order
                    % format for Unix systems (i.e., L, big endian)
                    fwrite(fid, dictionary_size, 'double');
                    fclose(fid);
                    fid = fopen(dst, 'a', 'L');
                    fwrite(fid, dictionary, 'double');
                    fclose(fid);
                elseif strcmp(obj.Ext,'.mat')
                    save(dst,'dictionary', 'dictionary_size');
                else
                    error('Error! Invalid extension.');
                end
                
            end
        end
    end
end
