classdef MyLLC < Features.GenericFeature
    
    properties
    end
    
    methods
        
        function obj = MyLLC(path, extension)
        
            p.llc.dictionary_size=256;
            p.llc.dictionary_path='/home/icub/Experiments/dictionaries/llc_coding.dict';
            p.llc.n_rand_features=2*1e5;
            
            p.llc.knn=5;
            
            p.llc.img_w=160;
            p.llc.img_h=160;
            
            if(p.mode=='robot')
                p.llc.img_w=320;
                p.llc.img_h=320;
            end
            
            p.llc.pyramid=[1 2 4];
            
            %for the locations
            gridSpacing= p.sift.step;
            patchSize=p.sift.scale;
            
            remX = mod(p.llc.img_w-patchSize,gridSpacing);
            offsetX = floor(remX/2)+1;
            remY = mod(p.llc.img_h-patchSize,gridSpacing);
            offsetY = floor(remY/2)+1;
            
            [gridX,gridY] = meshgrid(offsetX:gridSpacing:p.llc.img_w-patchSize+1, offsetY:gridSpacing:p.llc.img_h-patchSize+1);
            
            locx=gridX(:) + patchSize/2 - 0.5;
            locy=gridY(:) + patchSize/2 - 0.5;
            p.llc.locs=[locx locy]';
        end
        
        function descriptors = extract_image(obj, modality, src, dst)
          
            if (strcmp(modality,'file_file') || strcmp(modality,'file_wspace') || strcmp(modality,'file_both'))
                % load src
            elseif(strcmp(modality,'wspace_file') || strcmp(modality,'wspace_wspace') || strcmp(modality,'wspace_both'))
                % use directlysrc
            end
            
            % ...
            
            descriptors = descriptors';
            if (strcmp(modality,'file_file') || strcmp(modality,'wspace_file') || strcmp(modality,'file_both') || strcmp(modality,'wspace_both'))
                save(dst,'descriptors');     
            end
        end
     
        function p=code_llc_dataset(p)
            
            %if asked to ignore... ignore.
            if(isfield(p,'ignore_sift') || isfield(p,'ignore_llc'))
                return;
            end
            
            %load the dictionary
            l=load(p.llc.dictionary_path,'-mat');
            p.llc.dictionary=l.dictionary;
            
            %count the number of examples that will be coded
            n_samples=length(p.sift.registry);
            
            %create the empty matrix for the coded vectors
            %spatial bins on each level
            pBins = p.llc.pyramid.^2;
            % total spatial bins
            tBins = sum(pBins);
            p.llc.codes=zeros(size(p.llc.dictionary,2)*tBins,n_samples);
            
            fprintf('LLC Coding\n');
            fprintf('0%%              100%%\n');
            percent_module=uint32(n_samples/20);
            for idx_sample = 1 : n_samples
                
                if mod(idx_sample-1,percent_module)==0
                    fprintf('*');
                end
                
                %set the temporary descriptor in the parameter structure
                l=load(p.sift.registry{idx_sample}{end});
                
                
                %go yang!!
                p.llc.codes(:,idx_sample)=yang_llc(l.tmp_descriptors,p.llc.locs,p.llc.dictionary,p.llc.pyramid,p.llc.knn,p.llc.img_w,p.llc.img_h,1,0);
            end
            fprintf('\n\n');
            
            codes=p.llc.codes;
            save(p.llc.codes_path,'codes','-v7.3');
        end
        
        function p=dict_llc(p)
            %if asked to ignore... ignore.
            if(isfield(p,'ignore_sift') || isfield(p,'ignore_llc'))
                return;
            end
            
            %subsample the descriptors that will be used to learn the dictionary
            tmp_desc = vl_colsubset(p.sift.desc,p.llc.n_rand_features);
            
            
            [p.llc.dictionary, idx]=vl_kmeans(tmp_desc,p.llc.dictionary_size);
            
            for k=1:size(p.llc.dictionary,2)
                v=p.llc.dictionary(:,k);
                v=norm(v);
                p.llc.dictionary(:,k)=p.llc.dictionary(:,k)./v;
            end
            
            
            %save the dictionary
            dictionary=p.llc.dictionary;
            save(p.llc.dictionary_path,'dictionary');
        end
        
        function [Coeff] = LLC_coding_appr(B, X, knn, beta)
            
            % ========================================================================
            % USAGE: [Coeff]=LLC_coding_appr(B,X,knn,lambda)
            % Approximated Locality-constraint Linear Coding
            %
            % Inputs
            %       B       -M x d codebook, M entries in a d-dim space
            %       X       -N x d matrix, N data points in a d-dim space
            %       knn     -number of nearest neighboring
            %       lambda  -regulerization to improve condition
            %
            % Outputs
            %       Coeff   -N x M matrix, each row is a code for corresponding X
            %
            % Jinjun Wang, march 19, 2010
            % ========================================================================
            
            if ~exist('knn', 'var') || isempty(knn),
                knn = 5;
            end
            
            if ~exist('beta', 'var') || isempty(beta),
                beta = 1e-4;
            end
            
            nframe=size(X,1);
            nbase=size(B,1);
            
            % find k nearest neighbors
            XX = sum(X.*X, 2);
            BB = sum(B.*B, 2);
            D  = repmat(XX, 1, nbase)-2*X*B'+repmat(BB', nframe, 1);
            IDX = zeros(nframe, knn);
            for i = 1:nframe,
                d = D(i,:);
                [dummy, idx] = sort(d, 'ascend');
                IDX(i, :) = idx(1:knn);
            end
            
            % llc approximation coding
            II = eye(knn, knn);
            Coeff = zeros(nframe, nbase);
            for i=1:nframe
                idx = IDX(i,:);
                z = B(idx,:) - repmat(X(i,:), knn, 1);           % shift ith pt to origin
                C = z*z';                                        % local covariance
                C = C + II*beta*trace(C);                        % regularlization (K>D)
                w = C\ones(knn,1);
                w = w/sum(w);                                    % enforce sum(w)=1
                Coeff(i,idx) = w';
            end

        end
    
        function [beta] = yang_llc(feaSet, locs, B, pyramid, knn,img_width,img_height,pooling, pascal,img)
            %================================================
            %
            % Usage:
            % Compute the linear spatial pyramid feature using sparse coding.
            %
            % Inputss:
            % feaSet        -structure defining the feature set of an image
            %                   .feaArr     local feature array extracted from the
            %                               image, column-wise
            %                   .x          x locations of each local feature, 2nd
            %                               dimension of the matrix
            %                   .y          y locations of each local feature, 1st
            %                               dimension of the matrix
            %                   .width      width of the image
            %                   .height     height of the image
            % B             -sparse dictionary, column-wise
            % gamma         -sparsity regularization parameter
            % pyramid       -defines structure of pyramid
            %
            % Output:
            % beta          -multiscale max pooling feature
            %
            % Written by Jianchao Yang @ NEC Research Lab America (Cupertino)
            % Mentor: Kai Yu
            % July 2008
            %
            % Revised May. 2010
            %===============================================
            if(~exist('pooling','var') || isempty(pooling))
                pooling=1;
            end
            
            if(~exist('pascal','var') || isempty(pascal))
                pascal=0;
            end
            
            if(size(feaSet)==0)
                beta=zeros(size(B,2),1);
                return;
            end
            dSize = size(B, 2);
            nSmp = size(feaSet, 2);
            
            
            
            
            [sc_codes]=LLC_coding_appr(B',feaSet',knn);
            %[sc_codes]=sean_coding(B,feaSet,knn);
            sc_codes=sc_codes';
            
            
            
            
            if(pooling==0)
                beta=sc_codes;
                return;
            end
            
            % spatial levels
            pLevels = length(pyramid);
            % spatial bins on each level
            pBins = pyramid.^2;
            % total spatial bins
            tBins = sum(pBins);
            
            beta = zeros(dSize, tBins);
            bId = 0;
            cnt=1;
            
            if(~pascal)
                if(isempty(locs))
                    beta = max(sc_codes, [], 2);
                else
                    for iter1 = 1:pLevels,
                        
                        nBins = pBins(iter1);
                        
                        wUnit = img_width / pyramid(iter1);
                        hUnit = img_height / pyramid(iter1);
                        
                        % find to which spatial bin each local descriptor belongs
                        xBin = ceil(locs(1,:) / wUnit);
                        yBin = ceil(locs(2,:) / hUnit);
                        idxBin = (yBin - 1)*pyramid(iter1) + xBin;
                        
                        for iter2 = 1:nBins,
                            bId = bId + 1;
                            sidxBin = find(idxBin == iter2);
                            if isempty(sidxBin),
                                continue;
                            end
                            beta(:, bId) = max(sc_codes(:, sidxBin), [], 2);
                        end
                    end
                    
                    % Sqrt-l1 Normalization per pyramid level
                    %{
        tmp=beta(:,cnt:(cnt+nBins-1));
        tmp=tmp(:);
        beta(:,cnt:(cnt+nBins-1))=sqrt(abs(beta(:,cnt:(cnt+nBins-1))/sum(abs(tmp))));
        cnt=cnt+nBins;
                    %}
                end
            else
                pyramid=[1 2];
                % spatial levels
                pLevels = length(pyramid);
                % spatial bins on each level
                pBins = pyramid.^2;
                % total spatial bins
                tBins = sum(pBins);
                
                beta = zeros(dSize, 8);
                for iter1 = 1:pLevels,
                    
                    nBins = pBins(iter1);
                    
                    wUnit = img_width / pyramid(iter1);
                    hUnit = img_height / pyramid(iter1);
                    
                    % find to which spatial bin each local descriptor belongs
                    xBin = ceil(locs(1,:) / wUnit);
                    yBin = ceil(locs(2,:) / hUnit);
                    idxBin = (yBin - 1)*pyramid(iter1) + xBin;
                    
                    for iter2 = 1:nBins,
                        bId = bId + 1;
                        sidxBin = find(idxBin == iter2);
                        if isempty(sidxBin),
                            continue;
                        end
                        beta(:, bId) = max(sc_codes(:, sidxBin), [], 2);
                    end
                end
                
                hUnit = img_height /3;
                nBins=3;
                yBin = ceil(locs(2,:) / hUnit);
                idxBin = (yBin);
                for iter2 = 1:nBins,
                    bId = bId + 1;
                    sidxBin = find(idxBin == iter2);
                    if isempty(sidxBin),
                        continue;
                    end
                    beta(:, bId) = max(sc_codes(:, sidxBin), [], 2);
                end
            end
            
            
            beta = beta(:);
            beta = beta./sqrt(sum(beta.^2));
        end
end

