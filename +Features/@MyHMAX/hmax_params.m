function p = hmax_params(params)

if isfield(params,'NScales')
    NScales = params.NScales;
else
    NScales=8;
end

if isfield(params,'ScaleFactor')
    ScaleFactor = params.ScaleFactor;
else
    ScaleFactor = 2^0.2; 
end

if isfield(params,'NOrientations')
    NOrientations = params.NOrientations;
else
    NOrientations = 8;
end

if isfield(params,'S2RFCount')
    S2RFCount = params.S2RFCount;
else
    S2RFCount = [4 8 12];
end

if isfield(params,'BSize')
    BSize = params.BSize;
else
    BSize = 256;
end



p.groups = {};

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name = 'input';
c.type = 'input';
c.size = [400 600];

p.groups{end + 1} = c;
p.input = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name        = 'scale';
c.type        = 'scale';
c.pg          = p.input;
c.baseSize    = [BSize BSize];
c.scaleFactor = ScaleFactor;
c.numScales   = NScales;

p.groups{end + 1} = c;
p.scale = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name    = 's1';
c.type    = 'sdNDP';
c.pg      = p.scale;
c.rfCount = 11;
c.rfStep  = 1;
c.zero    = 1; 
c.thres   = 0.15; 
c.abs     = 1; 
c.fCount  = NOrientations;
c.fParams = {'gabor', 0.3, 5.6410, 4.5128};

p.groups{end + 1} = c;
p.s1 = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name    = 'c1';
c.type    = 'cMax';
c.pg      = p.s1;
c.sCount  = 2;
c.sStep   = 1;
c.rfCount = 10;
c.rfStep  = 5;

p.groups{end + 1} = c;
p.c1 = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name    = 's2';
c.type    = 'sdNDP';
c.pg      = p.c1;
c.rfCount = S2RFCount;
c.rfStep  = 1;
c.zero    = 1;
c.thres   = 0.00001;
c.abs     = 1;

p.groups{end + 1} = c;
p.s2 = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name    = 'c2';
c.type    = 'cMax';
c.pg      = p.s2;
c.sCount  = 4;
c.sStep   = 3;
c.rfCount = inf;

p.groups{end + 1} = c;
p.c2 = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

return;
