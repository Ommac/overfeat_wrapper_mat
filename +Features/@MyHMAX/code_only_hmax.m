


addpath(genpath('.'));

%%
%Create the dictionary

%HMAX
p=struct;
p=init_hmax(p);

p.hier.registry=load_registry('/home/icub/Experiments/registries/registry_dict.txt');

display('Creating Dictionary HMAX');
p=dict_hmax(p);


%%
%Coding

%code the training set
p.hier.registry=load_registry('/home/icub/Experiments/registries/registry_train.txt');
p.hmax.codes_path='/home/icub/Experiments/codes/codes_hmax_train.codes';

display('Coding Training HMAX');
code_hmax_dataset(p);

%code the test set
p.hier.registry=load_registry('/home/icub/Experiments/registries/registry_test.txt');
p.hmax.codes_path='/home/icub/Experiments/codes/codes_hmax_test.codes';

display('Coding Test HMAX');
code_hmax_dataset(p);


















