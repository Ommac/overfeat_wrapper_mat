function p=code_hmax_dataset(p)
    
    %if asked to ignore... ignore.
    if(isfield(p,'ignore_hmax'))
        return;
    end
      
    tmp_struct=load(p.hmax.dictionary_path,'-mat');
    p.hmax.lib=tmp_struct.save_struct.lib;
    p.hmax.params=tmp_struct.save_struct.params;
    
    n_samples=length(p.hier.registry);
    
    p.hmax.codes=zeros(p.hmax.feature_size,n_samples);
    
    fprintf('Coding HMAX\n');

    m = hmax.Model(p.hmax.params,p.hmax.lib);
    cns('init', m, p.hmax.mode);


    fprintf('0%%              100%%\n');
    percent_module=uint32(n_samples/20);
    for idx_sample = 1 : n_samples
        
        if mod(idx_sample-1,percent_module)==0
            fprintf('*');
        end
        hmax_input.Load(m,m.input,p.hier.registry{idx_sample}{end});
        cns('run');

        c2 = cns('get', -m.c2, 'val');
        c2 = cat(1, c2{:});

        p.hmax.codes(:,idx_sample)=c2;        
    end
    fprintf('\n\n');


    cns('done');
    
    codes=p.hmax.codes;
    save(p.hmax.codes_path,'codes','-v7.3');
end









