function p = init_hmax(p)

    p.hmax=struct;
    p.hmax.feature_size=4096;
    p.hmax.num_features=uint32(p.hmax.feature_size/2);
    
    p.hmax.NScales       = 8;
    p.hmax.ScaleFactor   = 2^0.2; 
    p.hmax.NOrientations = 8;
    p.hmax.S2RFCount     = [4 8 12];
    p.hmax.BSize         = 256;

    p.hmax.dictionary_path='/home/icub/Experiments/dictionaries/hmax.dict';
    p.hmax.mode='cpu';
    p.hmax.params = hmax_params(p.hmax);


end

