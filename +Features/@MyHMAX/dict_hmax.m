
function p=dict_hmax(p)

    %if asked to ignore... ignore.
    if(isfield(p,'ignore_hmax'))
        return;
    end
    
    p.hmax.lib = struct;
    m = hmax.Model(p.hmax.params,p.hmax.lib);    
    cns('init', m, p.hmax.mode);
    
    count = min(length(p.hier.registry), p.hmax.num_features);
    
    d = hmax_s.EmptyDict(m, m.s2, p.hmax.num_features);

    fprintf('0%%              100%%\n');
    percent_module=uint32(count/20);
    for i = 1 : count

        num_samples = floor(p.hmax.num_features / count);
        if i <= mod(p.hmax.num_features, count), num_samples = num_samples + 1; end

        if mod(i-1,percent_module)==0
            fprintf('*');
        end

        hmax_input.Load(m, m.input, p.hier.registry{i}{end});
        cns('run');

        d = hmax_s.SampleFeatures(m, m.s2, d, num_samples);
    end
    fprintf('\n\n');

    cns('done');

    d = hmax_s.SortFeatures(m, m.s2, d);

    if cns_istype(m, -m.s2, 'ss')
        d = hmax_ss.SparsifyDict(m, m.s2, d);
    end

    p.hmax.lib.groups{m.s2} = d;
    
    %%save the model
    save_struct=struct;
    save_struct.lib=p.hmax.lib;
    save_struct.params=p.hmax.params;
    
    save(p.hmax.dictionary_path,'save_struct');

end