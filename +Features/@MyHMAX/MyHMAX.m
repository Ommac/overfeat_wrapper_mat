classdef MyHMAX < Features.GenericFeature
    
    properties
    end
    
    methods
        
        function obj = MyHMAX(path, extension)
            
            p.hmax=struct;
            p.hmax.feature_size=4096;
            p.hmax.num_features=uint32(p.hmax.feature_size/2);
            
            p.hmax.NScales       = 8;
            p.hmax.ScaleFactor   = 2^0.2;
            p.hmax.NOrientations = 8;
            p.hmax.S2RFCount     = [4 8 12];
            p.hmax.BSize         = 256;
            
            p.hmax.dictionary_path='/home/icub/Experiments/dictionaries/hmax.dict';
            p.hmax.mode='cpu';
            p.hmax.params = hmax_params(p.hmax);
        end
        function p = hmax_params(params)
            
            if isfield(params,'NScales')
                NScales = params.NScales;
            else
                NScales=8;
            end
            
            if isfield(params,'ScaleFactor')
                ScaleFactor = params.ScaleFactor;
            else
                ScaleFactor = 2^0.2;
            end
            
            if isfield(params,'NOrientations')
                NOrientations = params.NOrientations;
            else
                NOrientations = 8;
            end
            
            if isfield(params,'S2RFCount')
                S2RFCount = params.S2RFCount;
            else
                S2RFCount = [4 8 12];
            end
            
            if isfield(params,'BSize')
                BSize = params.BSize;
            else
                BSize = 256;
            end
            
            
            
            p.groups = {};
            
            %-----------------------------------------------------------------------------------------------------------------------
            
            c = struct;
            c.name = 'input';
            c.type = 'input';
            c.size = [400 600];
            
            p.groups{end + 1} = c;
            p.input = numel(p.groups);
            
            %-----------------------------------------------------------------------------------------------------------------------
            
            c = struct;
            c.name        = 'scale';
            c.type        = 'scale';
            c.pg          = p.input;
            c.baseSize    = [BSize BSize];
            c.scaleFactor = ScaleFactor;
            c.numScales   = NScales;
            
            p.groups{end + 1} = c;
            p.scale = numel(p.groups);
            
            %-----------------------------------------------------------------------------------------------------------------------
            
            c = struct;
            c.name    = 's1';
            c.type    = 'sdNDP';
            c.pg      = p.scale;
            c.rfCount = 11;
            c.rfStep  = 1;
            c.zero    = 1;
            c.thres   = 0.15;
            c.abs     = 1;
            c.fCount  = NOrientations;
            c.fParams = {'gabor', 0.3, 5.6410, 4.5128};
            
            p.groups{end + 1} = c;
            p.s1 = numel(p.groups);
            
            %-----------------------------------------------------------------------------------------------------------------------
            
            c = struct;
            c.name    = 'c1';
            c.type    = 'cMax';
            c.pg      = p.s1;
            c.sCount  = 2;
            c.sStep   = 1;
            c.rfCount = 10;
            c.rfStep  = 5;
            
            p.groups{end + 1} = c;
            p.c1 = numel(p.groups);
            
            %-----------------------------------------------------------------------------------------------------------------------
            
            c = struct;
            c.name    = 's2';
            c.type    = 'sdNDP';
            c.pg      = p.c1;
            c.rfCount = S2RFCount;
            c.rfStep  = 1;
            c.zero    = 1;
            c.thres   = 0.00001;
            c.abs     = 1;
            
            p.groups{end + 1} = c;
            p.s2 = numel(p.groups);
            
            %-----------------------------------------------------------------------------------------------------------------------
            
            c = struct;
            c.name    = 'c2';
            c.type    = 'cMax';
            c.pg      = p.s2;
            c.sCount  = 4;
            c.sStep   = 3;
            c.rfCount = inf;
            
            p.groups{end + 1} = c;
            p.c2 = numel(p.groups);
            
            %-----------------------------------------------------------------------------------------------------------------------
            
        end
             
        function descriptors = extract_image(obj, modality, src, dst)
          
            if (strcmp(modality,'file_file') || strcmp(modality,'file_wspace') || strcmp(modality,'file_both'))
                % load src
            elseif(strcmp(modality,'wspace_file') || strcmp(modality,'wspace_wspace') || strcmp(modality,'wspace_both'))
                % use src
            end
            
            % ...
            
            descriptors = descriptors';
            if (strcmp(modality,'file_file') || strcmp(modality,'wspace_file') || strcmp(modality,'file_both') || strcmp(modality,'wspace_both'))
                save(dst,'descriptors');     
            end
        end
  
        function p=dict_hmax(p)
            
            %if asked to ignore... ignore.
            if(isfield(p,'ignore_hmax'))
                return;
            end
            
            p.hmax.lib = struct;
            m = hmax.Model(p.hmax.params,p.hmax.lib);
            cns('init', m, p.hmax.mode);
            
            count = min(length(p.hier.registry), p.hmax.num_features);
            
            d = hmax_s.EmptyDict(m, m.s2, p.hmax.num_features);
            
            fprintf('0%%              100%%\n');
            percent_module=uint32(count/20);
            for i = 1 : count
                
                num_samples = floor(p.hmax.num_features / count);
                if i <= mod(p.hmax.num_features, count), num_samples = num_samples + 1; end
                
                if mod(i-1,percent_module)==0
                    fprintf('*');
                end
                
                hmax_input.Load(m, m.input, p.hier.registry{i}{end});
                cns('run');
                
                d = hmax_s.SampleFeatures(m, m.s2, d, num_samples);
            end
            fprintf('\n\n');
            
            cns('done');
            
            d = hmax_s.SortFeatures(m, m.s2, d);
            
            if cns_istype(m, -m.s2, 'ss')
                d = hmax_ss.SparsifyDict(m, m.s2, d);
            end
            
            p.hmax.lib.groups{m.s2} = d;
            
            %%save the model
            save_struct=struct;
            save_struct.lib=p.hmax.lib;
            save_struct.params=p.hmax.params;
            
            save(p.hmax.dictionary_path,'save_struct');
            
        end
        
        function p=code_hmax_dataset(p)
            
            %if asked to ignore... ignore.
            if(isfield(p,'ignore_hmax'))
                return;
            end
            
            tmp_struct=load(p.hmax.dictionary_path,'-mat');
            p.hmax.lib=tmp_struct.save_struct.lib;
            p.hmax.params=tmp_struct.save_struct.params;
            
            n_samples=length(p.hier.registry);
            
            p.hmax.codes=zeros(p.hmax.feature_size,n_samples);
            
            fprintf('Coding HMAX\n');
            
            m = hmax.Model(p.hmax.params,p.hmax.lib);
            cns('init', m, p.hmax.mode);
            
            
            fprintf('0%%              100%%\n');
            percent_module=uint32(n_samples/20);
            for idx_sample = 1 : n_samples
                
                if mod(idx_sample-1,percent_module)==0
                    fprintf('*');
                end
                hmax_input.Load(m,m.input,p.hier.registry{idx_sample}{end});
                cns('run');
                
                c2 = cns('get', -m.c2, 'val');
                c2 = cat(1, c2{:});
                
                p.hmax.codes(:,idx_sample)=c2;
            end
            fprintf('\n\n');
            
            
            cns('done');
            
            codes=p.hmax.codes;
            save(p.hmax.codes_path,'codes','-v7.3');
        end
        
    end
    
end

