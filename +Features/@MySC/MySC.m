classdef MySC < Features.GenericFeature
    
    properties
        DictionaryDim
        
        Beta
        Gamma
        nIter
        
        Pyramid
        FeatsPerBin
    end
    
    methods

        function obj = MySC(ext, dict_dim, pyramid, beta, gamma, n_iter)
            
            obj = obj@Features.GenericFeature(ext);
            obj.DictionaryDim = dict_dim;
            
            obj.Beta = beta;
            obj.Gamma = gamma;
            obj.nIter = n_iter;
            
            if (isempty(pyramid))
                obj.Pyramid = [1 2 4;1 2 4]; % caltech-pyramid
            else
                obj.Pyramid = pyramid;
            end

        end
        
        function dictionary = dictionarize_matrix(obj, features, mod_out, dst)
            
            feature_dim = size(features,1);

            [dictionary, ~, ~] = reg_sparse_coding(features, obj.DictionaryDim, eye(obj.DictionaryDim), obj.Beta, obj.Gamma, obj.nIter);
    
            dictionary_size = size(dictionary);
            
            if (strcmp(mod_out, 'file') || strcmp(mod_out, 'both'))
                
                if strcmp(obj.Ext,'.bin')
                    fid = fopen(dst, 'w', 'L');
                    % append modality
                    % column major order
                    % format for Unix systems (i.e., L, big endian)
                    fwrite(fid, dictionary_size, 'double');
                    fclose(fid);
                    fid = fopen(dst, 'a', 'L');
                    fwrite(fid, dictionary, 'double');
                    fclose(fid);
                elseif strcmp(obj.Ext,'.mat')
                    save(dst,'dictionary', 'dictionary_size');
                else
                    error('Error! Invalid extension.');
                end

            end
        end
        
        function [featsPerBin] = feats_per_bin(obj, pyramid,im_size,grid)
            
            pyramid_w = pyramid(1,:);
            pyramid_h = pyramid(2,:);
            
            nLevels = size(pyramid,2);
            % spatial bins on each level
            binsPerLevel = pyramid_w.*pyramid_h;
            % total spatial bins
            nBins = sum(binsPerLevel);
            
            featsPerBin = cell(1,nBins);
            
            bId = 0;
            for l = 1:nLevels
                
                bin_w = im_size(2) / pyramid_w(l);
                bin_h = im_size(1) / pyramid_h(l);
                
                % find to which spatial bin each local descriptor belongs
                bin_x = ceil(grid(1,:) / bin_w);
                bin_y = ceil(grid(2,:) / bin_h);
                bin_idx = (bin_y-1)*pyramid(1,l) + bin_x;
                
                for b=1:binsPerLevel(l)
                    bId = bId + 1;
                    indices = find(bin_idx == b);
                    if isempty(indices),
                        continue;
                    end
                    
                    featsPerBin{bId} = indices;
                end
            end
        end
        
        function [feat, grid, src_feat_size, im_size] = extract_image(obj, mod_in, mod_out, src, src_grid, im_size, dst)
            
            if isempty(obj.Dictionary)
                error('Error! Missing Dictionary in calling object.');
            end
            
            if (strcmp(mod_in,'file'))
                
                if strcmp(obj.Ext,'.bin')
                    
                    fid = fopen(src, 'r', 'L');
                    % append modality
                    % column major order (transpose before writing)
                    % format for Unix systems (i.e., L, big endian)
                    im_size = fread(fid, [2 1], 'double');
                    src_feat_size = fread(fid, [2 1], 'double');
                    src_feat = fread(fid, src_feat_size', 'double');
                    grid_size = fread(fid, [2 1], 'double');
                    grid = fread(fid, grid_size', 'double');
                    fclose(fid);
                    
                elseif strcmp(obj.Ext,'.mat')
                    
                    input = load(src, '-mat', 'feat', 'grid', 'feat_size', 'grid_size', 'im_size');
                    src_feat = input.feat;
                    src_feat_size = input.feat_size;
                    grid = input.grid;
                    grid_size = input.grid_size;
                    im_size = input.im_size;
 
                else
                    error('Error! Invalid extension.');
                end
 
            end
            if (strcmp(mod_in,'wspace'))
                src_feat = src;
                src_feat_size = size(src_feat);
                grid = src_grid; 
                grid_size = size(grid);
            end

            % for each bin, idxs of the grid points falling inside it
            obj.FeatsPerBin = obj.feats_per_bin(obj.Pyramid,im_size,grid);
   
            % a SC code from an image is obj.DictionaryDim*nBins x 1
            feat = code_sc(src_feat, obj.Pyramid, obj.Gamma, obj.Dictionary, im_size, grid);
   
            if (strcmp(mod_out,'file') || strcmp(mod_out,'both'))
                feat_size = size(feat); 
                
                if strcmp(obj.Ext,'.bin')
                    fid = fopen(dst, 'w', 'L');
                    % append modality
                    % column major order
                    % format for Unix systems (i.e., L, big endian)
                    fwrite(fid, im_size, 'double');
                    fclose(fid);
                    fid = fopen(dst, 'a', 'L');
                    fwrite(fid, feat_size, 'double');
                    fwrite(fid, feat, 'double');
                    fwrite(fid, grid_size, 'double');
                    fwrite(fid, grid, 'double');
                    fclose(fid);
                elseif strcmp(obj.Ext,'.mat')
                    save(dst,'feat', 'grid', 'feat_size', 'grid_size', 'im_size');
                else
                    error('Error! Invalid extension.');
                end

            end
            
        end
  
    end
    
end
