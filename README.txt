Overfeat Feature Extractor - MATLAB wrapper code -

+Feature: package containing the classes for the feature extraction stage
 - GenericFeature, template for the derived following classes
   - MyBOW, Bag Of Words
   - MyFV, Fisher Vectors
   - MyHMAX, HMAX model
   - MyLLC, Locality-constrained Linear Coding
   - MyPCA, Principal Component Analysis
   - MySC, Sparse Coding 
   - MySIFT, SIFT
 
main.m: example of usage of the classes in the Feature package
