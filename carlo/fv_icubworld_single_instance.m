%% Demonstration script for running a pipeline on iCubWorld1.0
% - SIFT features are extracted from the images in the dataset
% - PCA is applied to reduce the original dimensionality of SIFTs
% - Fisher Vectors (FV) are extracted using Spatial Pyramid

%% VLFeat setup

curr_dir = pwd;
cd([getenv('VLFEAT_ROOT') '/toolbox']);
vl_setup;
cd(curr_dir);
clear curr_dir;

% %% GURLS setup
% 
% run('/home/icub/Dev/GURLS/gurls/utils/gurls_install.m');

%% MATLAB path update

% add the folder containing the '+Features' package to the path
%FEATURES_DIR = 'C:\Users\Giulia\REPOS\objrecpipe_mat';
FEATURES_DIR = '/Users/cciliber/Dev/objrecpipe_mat';
addpath(FEATURES_DIR);
% if you want to add it permanently
% savepath 

% add the current working directory to the path
addpath(genpath('.'));

% common root path
root_path = '/Users/cciliber/Dev/experiments/icubworld_single_instance';

%% iCubWorld2.1

version = 2.1;
datasets_folder = '/Users/cciliber/Dev/datasets';
dataset_path = fullfile(datasets_folder, 'iCubWorld_SingleInstance');
dataset_code_path = fullfile(dataset_path, 'ICUBcode');
dataset_images_path = fullfile(dataset_path, 'PPMimages', modality);

% set output directory
experiments_path = fullfile(datasets_folder, 'iCubWorld_SingleInstance_experiments');

% either choose a name for the full registry file
registry_full = 'icub_si_registry.txt';
% or choose a partial registry file if want to process a subset of the dataset
registry_partial = 'icub_si_registry_partial.txt';

% assign image format
dataset_extension = [];

% initialize VOC options
addpath(dataset_code_path);

ICUBWORLDopts = ICUBWORLDinit(datasets_folder, version, []);

%% Dataset initialization

registry_path_full = fullfile(experiments_path, registry_full);
registry_path_partial = fullfile(experiments_path, registry_partial);

% folder in which store the X and y matrices for the classifier
classifier_input_path = fullfile(experiments_path, 'classifier_input');

% create dataset object
dataset = Features.MyDataset(dataset_extension);

% scan the path of folders and
% - assign .RootPath = path
% - assign .RegistryPath = reg_path and create registry file
% - assign .Registry
% - assign .Tree
% - assign .ExampleCount
dataset.assign_registry_and_tree_from_folder(dataset_images_path, registry_path_full);

% read a registry file and
% - create .Tree
% - create .ExampleCount
% - create .Registry
% - create .RegistryPath = reg_path
%dataset.assign_registry_and_tree_from_file(registry_path_partial);

imgsets = {'train', 'test'};
% filter the whole-dataset set-specific registry files 
% with the list in obj.Registry (that can be partial)
% as a result
% 1) obj.Registry is splitted in set-specific registries
% that are assigned to obj.Registry_<set> and saved as file at obj.RegistryPath_<set>
% 2) corresponding y vectors are created
% if modality_out = 'file' it assigns obj.YPath = classifier_input_path 
% if modality_out = 'wspace' it assigns obj.Y_<set>
% --> the fields obj.Registry and obj.RegistryPath must be set correctly
Yfile_ext = '.mat';
dataset.create_y_and_registries_fromICUBWORLD('file', Yfile_ext, classifier_input_path, ICUBWORLDopts, imgsets);
%dataset.create_y_and_registries_fromICUBWORLD('both', Yfile_ext, classifier_input_path, ICUBWORLDopts, imgsets);
%dataset.create_y_and_registries_fromICUBWORLD('wspace', '', '', ICUBWORLDopts, imgsets);

%% Pipeline initialization for 'train' and 'test' phases

dataset_train = Features.GenericFeature(dataset_extension);
dataset_test = Features.GenericFeature(dataset_extension);

dataset_train.assign_registry_and_tree_from_file(dataset.RegistryPath_train);
dataset_test.assign_registry_and_tree_from_file(dataset.RegistryPath_test{1});

%% SIFT 

sift_extension = '.bin'; % either '.bin' or '.mat'
sift_step = 16;
sift_scale = [8 12 16 24 32];

sift_train = Features.MySIFT(sift_extension, sift_step, sift_scale);
sift_test = Features.MySIFT(sift_extension, sift_step, sift_scale);

% read a registry file and
% - create .Tree
% - create .ExampleCount
% - create .Registry
% - create .RegistryPath = reg_path
sift_train.assign_registry_and_tree_from_file(dataset_train.RegistryPath);
sift_test.assign_registry_and_tree_from_file(dataset_test.RegistryPath);

sift_path = fullfile(experiments_path, 'sift');
sift_train_path = sift_path;
sift_test_path = sift_path;

dataset.RootPath = dataset_images_path;
dataset_train.RootPath = dataset.RootPath;
dataset_test.RootPath = dataset.RootPath;
% if modality_in = 'file' it loads features from prev_obj.RootPath 
% using prev_obj.Registry
% if modality_in = 'wspace' it uses prev_obj.Feat
% --> these fields must be set correctly
% no need for dictionary
% if modality_out = 'file' it assigns obj.RootPath = path
% if modality_out = 'wspace' it assigns obj.Feat
% EXAMPLE: 
% sift.extract(modality_in, '', modality_out, sift_path, dataset_object);
% with modality_in = 'file' (dataset images) 
% and modality_out = {'file', 'wspace', 'both'}
sift_train.extract('file', '', 'file', sift_train_path, dataset_train);

sift_test.extract('file', '', 'file', sift_test_path, dataset_test);

%% PCA

siftpca_extension = '.bin';
siftpca_dict_size = 64;
siftpca_n_randfeat = 2*1e5;

siftpca_train = Features.MyPCA(siftpca_extension, siftpca_dict_size, siftpca_n_randfeat);
siftpca_test = Features.MyPCA(siftpca_extension, siftpca_dict_size, siftpca_n_randfeat);

% read a registry file and
% - create .Tree
% - create .ExampleCount
% - create .Registry
% - create .RegistryPath = reg_path
siftpca_train.assign_registry_and_tree_from_file(sift_train.RegistryPath);
siftpca_test.assign_registry_and_tree_from_file(sift_test.RegistryPath);

siftpca_path = fullfile(experiments_path, 'siftpca');
siftpca_train_path = siftpca_path;
siftpca_test_path = siftpca_path;

% if modality_in = 'file' it loads features from prev_obj.RootPath using prev_obj.Registry
% if modality_in = 'wspace' it uses prev_obj.Feat
% --> these fields must be set correctly
% if modality_out = 'file' it assigns obj.DictionaryPath = fullfile(path, ['dictionary' obj.Ext]);
% if modality_out = 'wspace' it assigns obj.Dictionary
% EXAMPLE:
% siftpca.dictionarize(modality_in, modality_out, siftpca_path, sift_object);
% with modality_in = {'file', 'wspace'}
% and modality_out = {'file', 'wspace', 'both'}
sift_train.RootPath = sift_train_path;
sift_test.RootPath = sift_test_path;
siftpca_train.dictionarize('file', 'file', siftpca_train_path, sift_train);

% if modality_in = 'file' it loads features from prev_obj.RootPath 
% using prev_obj.Registry
% if modality_in = 'wspace' it uses prev_obj.Feat
% --> these fields must be set correctly
% if modality_dict = 'file' it loads the dictionary from obj.DictionaryPath
% if modality_dict = 'wspace' it uses obj.Dictionary
% --> these fields must be set correctly
% if modality_out = 'file' it assigns obj.RootPath = path
% if modality_out = 'wspace' it assigns obj.Feat
% EXAMPLE: 
% siftpca.extract(modality_in, '', modality_out, siftpca_path, sift_object);
% with modality_in/dict = {'file', 'wspace'}
% and modality_out = {'file', 'wspace', 'both'}
siftpca_train.extract('file', 'file', 'file', siftpca_train_path, sift_train);

% copy the input obj.Dictionary and obj.DictionaryPath
% in the same fields of calling object
siftpca_test.import_dictionary(siftpca_train, 'file');
siftpca_test.extract('file', 'file', 'file', siftpca_test_path, sift_test);

%% FISHER VECTOR

fv_extension = '.bin';
fv_dict_size = 20;
fv_n_randfeat = 2*1e5;
fv_pyramid = [1 2 4; 1 2 4];

fv_train = Features.MyFV(fv_extension, fv_dict_size, fv_n_randfeat, fv_pyramid);
fv_test = Features.MyFV(fv_extension, fv_dict_size, fv_n_randfeat, fv_pyramid);

% read a registry file and
% - create .Tree
% - create .ExampleCount
% - create .Registry
% - create .RegistryPath = reg_path
fv_train.assign_registry_and_tree_from_file(siftpca_train.RegistryPath);
fv_test.assign_registry_and_tree_from_file(siftpca_test.RegistryPath);

fv_path = fullfile(experiments_path, ['fv_d' num2str(fv_dict_size)]);
fv_train_path = fv_path;
fv_test_path = fv_path;

% if modality_in = 'file' it loads features from prev_obj.RootPath using prev_obj.Registry
% if modality_in = 'wspace' it uses prev_obj.Feat
% --> these fields must be set correctly
% if modality_out = 'file' it assigns obj.DictionaryPath = path
% if modality_out = 'wspace' it assigns obj.Dictionary
% EXAMPLE:
% fv.dictionarize(modality_in, modality_out, fv_path, siftpca_object);
% with modality_in = {'file', 'wspace'}
% and modality_out = {'file', 'wspace', 'both'}
siftpca_train.RootPath = siftpca_train_path;
siftpca_test.RootPath = siftpca_test_path;
fv_train.dictionarize('file', 'file', fv_train_path, siftpca_train);

% if modality_in = 'file' it loads features from prev_obj.RootPath 
% using prev_obj.Registry
% if modality_in = 'wspace' it uses prev_obj.Feat
% --> these fields must be set correctly
% if modality_dict = 'file' it loads the dictionary from obj.DictionaryPath
% if modality_dict = 'wspace' it uses obj.Dictionary
% --> these fields must be set correctly
% if modality_out = 'file' it assigns obj.RootPath = path
% if modality_out = 'wspace' it assigns obj.Feat
% EXAMPLE: 
% fv.extract(modality_in, '', modality_out, fv_path, siftpca_object);
% with modality_in/dict = {'file', 'wspace'}
% and modality_out = {'file', 'wspace', 'both'}
fv_train.extract('file', 'file', 'file', fv_train_path, siftpca_train);

% copy the input obj.Dictionary and obj.DictionaryPath
% in the same fields of calling object
fv_test.import_dictionary(fv_train,'file');
fv_test.extract('file', 'file', 'both', fv_test_path, siftpca_test);

%% Prepare X and y for CLASSIFIER

% load the features from bin/mat files into a single matrix (obj.Feat)
%fv_train.Ext = '.bin';
%fv_train.load_feat(fv_path);
% save obj.FeatSize = [dataset.Ntrain dataset.n_classes] and obj.Feat in a bin/mat file
fv_train.Ext = '.mat';
fv_train.save_feat_matrix(fullfile(classifier_input_path, 'fv100_Xtrain'));

% load the features from bin/mat files into a single matrix (obj.Feat)
% fv_test.Ext = '.bin';
% fv_test.load_feat(fv_path);
% save obj.FeatSize = [dataset.Ntrain dataset.n_classes] and obj.Feat in a bin/mat file
fv_test.Ext = '.mat';
fv_test.save_feat_matrix(fullfile(classifier_input_path, 'fv100_Xtest'));

% % load obj.FeatSize = [dataset.Ntrain dataset.n_classes] and obj.Feat from a binary file
% fv_train.load_feat_matrix(classifier_input_path);
% fv_test.load_feat_matrix(classifier_input_path);


% 
% imgsets = {'train', 'test'};
% dataset.load_y(classifier_input_path, '.bin', ICUBWORLDopts, imgsets);
% 
% %% CLASSIFIER
% 
% name = ['experiment_icubworld10_' modality '_fv_d' num2str(fv_dict_size)];
% 
% % default options
% opt = defopt(name);
% 
% % new options
% opt.hoproportion = 0.5;
% opt.nlambda = 5000;
% opt.kernel.type = 'linear';
% 
% opt.seq = {'kernel:linear','split:ho', 'paramsel:hodual', 'rls:dual', 'pred:dual', 'perf:precrec'};
% % 'paramsel:loocvdual'
% 
% opt.process{1} = [2,2,2,2,0,0];
% opt.process{2} = [3,3,3,3,2,2];
% 
% % TRAIN
% 
% fv_train.load_feat(fv_path);
% Xtr = fv_train.Feat';
% ytr = dataset.Y_train;
% 
% % subtract mean
% Xm = mean(Xtr,1);
% Xtr = Xtr - ones(size(Xtr,1),1)*Xm;
% %[Xtr] = norm_zscore(Xtr, ytr, opt); 
% 
% gurls (Xtr, ytr, opt, 1);
% 
% clear Xtr ytr fv_train.Feat;
% 
% % TEST
% 
% fv_test.load_feat(fv_path);
% Xte = fv_test.Feat';
% yte = dataset.Y_test{1};
% 
% % subtract mean
% Xte = Xte - ones(size(Xte,1),1)*Xm;
% %[Xte] = norm_testzscore(Xte, yte, opt); 
% 
% result = gurls (Xte, yte, opt, 2);
% [rec,prec,ap] = vl_pr(yte,result.pred);
% save(['experiment_icubworld10' modality '_fv_d' num2str(fv_dict_size) '_peformance.mat'], 'rec', 'prec', 'ap');
% 
% clear Xte yte fv_test.Feat;