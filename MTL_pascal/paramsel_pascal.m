function [best_lambda,lambda_path,ap_tr_values,ap_va_values] = paramsel_pascal(Ktr,ytr,Kva,yva,n_lambdas,penalty,n_rep_tr,perc_tr,n_rep_va,perc_va)

    if nargin<=5
       penaldy='TRACE';
    end

    if nargin<=6
       n_rep_tr = 1; 
       n_rep_va = 1;
       perc_tr = 1.0;
       perc_va = 1.0;
    end
    
    
    ntr = size(Ktr,1);
    subsample_num_tr = uint32(ntr*perc_tr); %get the percentage of training examples to take for every experiment
    
    lambda_path = span_lambdas(Ktr,n_lambdas);
   
    ap_tr_values = zeros(size(lambda_path));
    ap_va_values = zeros(size(lambda_path));
    
    best_lambda = lambda_path(end);
    best_ap = -1.0;
    
    for idx_l = 1:numel(lambda_path)
       
        mean_ap_tr=0.0;
        mean_ap_va=0.0;
        for idx_rep_tr = 1:n_rep_tr
            
            rnd_idx = randperm(ntr);
            rnd_idx((subsample_num_tr+1):end)=[];
            
            Ktr_exp = Ktr(rnd_idx,rnd_idx);
            ytr_exp = ytr(rnd_idx,:);
            Kva_exp = Kva(:,rnd_idx);
            
            [ap_va,ap_tr] = perform_MTL_experiment(Ktr_exp,ytr_exp,Kva_exp,yva,lambda_path(idx_l),penalty,n_rep_va,perc_va);
        
            mean_ap_tr=mean_ap_tr+ap_tr;
            mean_ap_va=mean_ap_va+ap_va;
            
        end
        
        mean_ap_tr=(1.0/n_rep_tr)*mean_ap_tr;
        mean_ap_va=(1.0/n_rep_va)*mean_ap_va;

        ap_tr_values(idx_l) = mean_ap_tr;
        ap_va_values(idx_l) = mean_ap_va;

        if mean_ap_va > best_ap
            best_ap = mean_ap_va;
            best_lambda = lambda_path(idx_l);
        end

    end

    

end


