
function [mean_ap_va,mean_ap_tr] = perform_MTL_experiment(Ktr,ytr,Kva,yva,lambda,penalty,n_rep_va,perc_va)

    nva = size(Kva,1);
    subsample_num_va=uint32(nva*perc_va);
    
    %solve the learning problem for the specified lambda
    [B,b,~] = RLS_MTL_train(Ktr,ytr,lambda,penalty);
    
    %error on the training set
    ypred_tr = Ktr*B+ones(size(Ktr,1),1)*b;

    %compute the ap on the training set according to the pascal challenge
    [~,~,ap_tr]=vl_pr(ytr,ypred_tr);
    mean_ap_tr = ap_tr.auc_pa08;
    %mean_ap_tr=compute_acc(ytr,ypred_tr);
    
    
    mean_ap_va = 0.0;    
    for idx_rep_va = 1:n_rep_va

        rnd_idx = randperm(nva);
        rnd_idx((subsample_num_va+1):end)=[];        
        
        %test the learned model on the validation set
        ypred_va = Kva(rnd_idx,:)*B+ones(subsample_num_va,1)*b;

        %compute the ap on the test set according to the pascal challenge
        [~,~,ap_va]=vl_pr(yva(rnd_idx,:),ypred_va);
        mean_ap_va = mean_ap_va + ap_va.auc_pa08;

        %mean_ap_va = mean_ap_va + compute_acc(yva(rnd_idx,:),ypred_va);
        
    end

    mean_ap_va = (1.0/n_rep_va)*mean_ap_va;
    
end
