function lambdas = span_lambdas(K,n_lambdas)

    n = size(K,1);

    eigvals = eig(K);
    eigvals = sort(eigvals,'descend');

    % maximum eigenvalue
    lmax = eigvals(1);

    lmin = max(min(lmax*1e-05, eigvals(end)), 200*sqrt(eps));

    powers = linspace(0,1,n_lambdas);
    lambdas = lmin.*(lmax/lmin).^(powers);
    lambdas = lambdas/n;
    
end