
clear all;

addpath(genpath('..'));

%% dataset specific parameters
load_path_train='/DATA/Carlo/experiment_pascal/learning_data/kernel_train.mat';
load_path_val='/DATA/Carlo/experiment_pascal/learning_data/kernel_val.mat';
load_path_trainval='/DATA/Carlo/experiment_pascal/learning_data/kernel_trainval.mat';
load_path_test='/DATA/Carlo/experiment_pascal/learning_data/kernel_test.mat';


%% [paramsel] - load data

train_data = load(load_path_train);
Ktr = train_data.K;
ytr = train_data.Y;
clear train_data;


display('loaded train');

val_data = load(load_path_val);
Kva = val_data.K;
yva = val_data.Y;
clear val_data;



display('loaded val');



% sub_n=100;
% rnd_idx = randperm(size(Ktr,1));
% rnd_idx = rnd_idx(1:sub_n);
% Ktr = Ktr(rnd_idx,rnd_idx);
% ytr = ytr(rnd_idx,:);
% 
% Kva = Kva(:,rnd_idx);
% 

%% [paramsel] - perform the parameter selection

penalty='SPARSE';
n_rep_tr=1;
perc_tr=1.0;
n_rep_va=1;
perc_va=1.0;

n_lambdas = 10;

display('starting paramsel');
[best_lambda,lambda_path,ap_tr,ap_va] = paramsel_pascal(Ktr,ytr,Kva,yva,n_lambdas,penalty,n_rep_tr,perc_tr,n_rep_va,perc_va);
display('paramsel done');

figure;
hold on;
plot(lambda_path,ap_tr,'r');
plot(lambda_path,ap_va,'b');
hold off;





%% clean up

clear Ktr ytr Kva yva;


%% [train/test] load data and compute kernels

train_data = load(load_path_trainval);
Ktr = train_data.K;
ytr = train_data.Y;
clear trainval_data;

display('loaded trainval');



val_data = load(load_path_test);
Kte = val_data.K;
yte = val_data.Y;
clear test_data;


display('loaded test');

% sub_n=1000;
% rnd_idx = randperm(size(Ktr,1));
% rnd_idx = rnd_idx(1:sub_n);
% Ktr = Ktr(rnd_idx,rnd_idx);
% ytr = ytr(rnd_idx,:);
% 
% Kte = Kte(:,rnd_idx);


%% [train]
display('training');
[B,b,A] = RLS_MTL_train(Ktr,ytr,best_lambda,penalty);
display('trained');

%% [test]

display('testing');
ypred = Kte*B + ones(size(Kte,1),1)*b;
[rec,prec,ap] = vl_pr(yte,ypred);








