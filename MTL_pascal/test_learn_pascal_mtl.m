
clear all;


T = 20;
d = 30;
ntr = 100;
nva = 150;
nte = 300;

I = 2*eye(T)-1;


%% [paramsel] - load data

Xtr = randn(ntr,d);
ytr = floor(rand(ntr,1)*T-eps)+1;
ytr = I(ytr,:);

Xva = randn(nva,d);
yva = floor(rand(nva,1)*T-eps)+1;
yva = I(yva,:);

%% [paramsel] - compute kernels
Kva = [Xtr;Xva]*Xtr';
Ktr = Kva(1:size(Xtr,1),:);
Kva(1:size(Xtr,1),:)=[];


%% [paramsel] - perform the parameter selection

penalty='TRACE';
n_rep_tr=1;
perc_tr=1.0;
n_rep_va=1;
perc_va=1.0;

n_lambdas = 100;

[best_lambda,lambda_path,ap_tr,ap_va] = paramsel_pascal(Ktr,ytr,Kva,yva,n_lambdas,penalty,n_rep_tr,perc_tr,n_rep_va,perc_va);

%% clean up

clear Ktr Kva;


%% [train/test] load data and compute kernels

Xtr = [Xtr;Xva];
ytr = [ytr;yva];

clear Xva yva;

Xte = randn(nte,d);
yte = floor(rand(nte,1)*T-eps)+1;
yte = I(yte,:);

Kte = [Xtr;Xte]*Xtr';
Ktr = Kte(1:size(Xtr,1),:);
Kte(1:size(Xtr,1),:)=[];

clear Xtr Xte;

%% [train]

[B,b,A] = RLS_MTL_train(Ktr,ytr,best_lambda,penalty);


%% [test]

ypred = Kte*B + ones(size(Kte,1),1)*b;
[rec,prec,ap] = vl_pr(yte,ypred);









