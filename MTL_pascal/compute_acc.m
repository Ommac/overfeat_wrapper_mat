
function acc = compute_acc(yte,ypred);

    [n,T] = size(yte);

    [drop,imageEstClass] = max(ypred, [], 2) ;
    [drop,imageClass]= max(yte,[],2);

    % Compute the confusion matrix
    idx = sub2ind([T,T],imageClass,imageEstClass) ;
    confus = zeros(T) ;
    confus = vl_binsum(confus, ones(size(idx)), idx) ;
    acc = 100 * mean(diag(confus)/n);

end