
%%
clear all;

%%
path='/DATA/Carlo/experiment_pascal/learning_data/';


%% save train

dataset_type = 'train';

load_path = fullfile(path,[dataset_type '.mat']);
save_path = fullfile(path,['kernel_' dataset_type '.mat']);
save_path_dist = fullfile(path,['dist_' dataset_type '.mat']);

data = load(load_path);
Xtr = data.X;
Y = data.Y;
clear data;

display(['loaded ' load_path]);

K = Xtr*Xtr';


save(save_path,'K','Y','-v7.3');

clear K;

D = square_distance(Xtr',Xtr');

save(save_path_dist,'D','Y','-v7.3');

clear D Y;



%% save val

dataset_type = 'val';

load_path = fullfile(path,[dataset_type '.mat']);
save_path = fullfile(path,['kernel_' dataset_type '.mat']);
save_path_dist = fullfile(path,['dist_' dataset_type '.mat']);

data = load(load_path);
Xva = data.X;
Y = data.Y;
clear data;

display(['loaded ' load_path]);

K = Xva*Xtr';


save(save_path,'K','Y','-v7.3');

clear K;

D = square_distance(Xva',Xtr');

save(save_path_dist,'D','Y','-v7.3');

clear D Y Xtr Xva;


%% save trainval

dataset_type = 'trainval';

load_path = fullfile(path,[dataset_type '.mat']);
save_path = fullfile(path,['kernel_' dataset_type '.mat']);
save_path_dist = fullfile(path,['dist_' dataset_type '.mat']);

data = load(load_path);
Xtr = data.X;
%Y = data.Y;
%clear data;

display(['loaded ' load_path]);

%K = Xtr*Xtr';


% save(save_path,'K','Y','-v7.3');
% 
% clear K;
% 
% D = square_distance(Xtr',Xtr');
% 
% save(save_path_dist,'D','Y','-v7.3');
% 
% clear D Y;

% save test

dataset_type = 'test';

load_path = fullfile(path,[dataset_type '.mat']);
save_path = fullfile(path,['kernel_' dataset_type '.mat']);
save_path_dist = fullfile(path,['dist_' dataset_type '.mat']);

data = load(load_path);
Xte = data.X;
Y = data.Y;
clear data;

display(['loaded ' load_path]);

K = Xte*Xtr';
display(['K done. Saving...']);

save(save_path,'K','Y','-v7.3');

clear K;



D = square_distance(Xte',Xtr');

display(['D done. Saving...']);


save(save_path_dist,'D','Y','-v7.3');

clear D Y Xtr Xte;


