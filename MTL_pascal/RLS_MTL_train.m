
function [B,b,A] = RLS_MTL_train(K,y,lambda,penalty)
    
    [n,T]=size(y);    
    
    %the projector orthogonal to 1
    P = (eye(n) - ones(n)/n);
    
    %center K and y
    PKP = P*K*P;
    PY = P*y;    

    Py = PY(:);
    
    UA=eye(T);
    da=ones(T,1);
    A=eye(T)/T;


    max_cnt = 100;
    delta = 1e-3 * norm(PY,'fro');
    
    for i=1:max_cnt;
        
        Ap=A;
        
        Ytilde = PY*UA;
        Ctilde = zeros(n,T);
        for t=1:T
            R = chol(da(t)*PKP+n*lambda*eye(n));
            Ctilde(:,t) = R\(R'\Ytilde(:,t));
        end
        
        B = Ctilde*diag(da)*UA';
        
        
        [UA,D]=eig(B'*PKP*B);
        D = max(0,abs(diag(double(D))));
                
        switch penalty
            
            case 'TRACE'                                
                D = sqrt(D);
                A = UA*diag(D)*UA';
                da = D;
                
            case 'FROBENIUS'
        
                D = (D*0.5).^(1.0/3.0);
                A = UA*diag(D)*UA';
                da = D;
        
            case 'SPARSE'
                
                sqrtBKB = UA*sqrt(diag(D))*UA';
                
                %cvx_quiet(true);
                
                cvx_begin
                    variable A(T,T);
                    variable C(T,T);
                    minimize ( trace(C) + sum(sum(abs(A))) );
                    
                    subject to
                        [A sqrtBKB; sqrtBKB C] == semidefinite(2*T,2*T);
                cvx_end
                
                %cvx_quiet(false);

                [UA,D]=eig(A);
                D = max(0,abs(diag(double(D))));
                da = D;
        end

        % Compute the value of the objective functional
        
        
        A(A~=0)=A(A~=0)/trace(A);
        
        if norm(Ap-A,'fro') < delta
            break;
        end
    end

    
    % compute the intercepts
    b = mean(y,1) - (1.0/n)*ones(1,n)*K*B;
    
end
