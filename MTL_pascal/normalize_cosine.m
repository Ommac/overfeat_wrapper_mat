function M_norm = normalize_cosine(M)

    n = size(M,1);

    M_norm=M;
    for i=1:n
       for j=1:n
          M_norm(i,j)=M(i,j)/sqrt( (M(i,i)*M(j,j)) );
          M_norm(j,i)=M_norm(i,j);
       end
    end

end